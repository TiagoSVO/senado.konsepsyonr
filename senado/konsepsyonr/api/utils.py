# coding=utf-8

def getCanvas(context):
    """
    Método que procura o primeiro filho do tipo canvas registrado
    """
    context = context.aq_inner
    if(context.portal_type != "konsep.canvas"):
        canvas = context.listFolderContents({'portal_type':'konsep.canvas'})
        if canvas and canvas[0]:
            return canvas[0]
        else:
            return None
    else:
        return context