# coding=utf-8
import json

from plone import api

from senado.konsepsyonr.api.utils import getCanvas

from five import grok
from senado.konsepsyonr.adapters.searcher import ISearcher
from senado.konsepsyonr.interfaces import ISenadoKonsepsyonrAddon
from zope.interface import Interface

grok.templatedir("templates")
grok.layer(ISenadoKonsepsyonrAddon)

class canvas(grok.View):
    grok.name('canvas')
    grok.require('zope2.View')
    grok.template('canvas')
    grok.context(Interface)

    def update(self, **kwargs):
        context = self.context
        canvas = getCanvas(context)
        abslt_url = canvas.absolute_url()
        self.canvas = canvas
        self.data_save = str(abslt_url) + '/save'
        self.data_search = str(abslt_url) + '/search'
        self.data_systems = json.dumps(self.canvas.data) if self.canvas.data else json.dumps([[{"k": [{"s": [12]}]}]])
        self.renderCanvas = self.canvas.renderCanvas(context=self.canvas.__parent__)
        self.isLogged = not api.user.is_anonymous()
        self.srcBootstrapJs = self.getBootstrapJs()
        self.srcKonsepsyonrJs = self.getKonsepsyonrJs()
        self.srcMainJs = self.getMainJs()

    def getBootstrapJs(self):
        return str(api.portal.get().absolute_url()) + '/++resource++senado.konsepsyonr/js/bootstrap.min.js'

    def getKonsepsyonrJs(self):
        return str(api.portal.get().absolute_url()) + '/++resource++senado.konsepsyonr/js/konsepsyonr.js'

    def getMainJs(self):
        return str(api.portal.get().absolute_url()) + '/++resource++senado.konsepsyonr/js/main.js'



class search(grok.View):
    grok.name('search')
    grok.context(Interface)

    def render(self, **kwargs):
        if not api.user.is_anonymous():
            request = self.request
            response = self.response

            response.setHeader('Content-Type', 'application/json; charset=utf8')
            query = request.form.get('text')
            searcher = ISearcher(self.context)
            result = searcher.query(query)

            return result

class save(grok.View):
    grok.name('save')
    grok.context(Interface)

    def render(self, **kwargs):
        if not api.user.is_anonymous():
            request = self.request

            data = json.dumps([[{"k": [{"s": [12]}]}]])
            dataRequest = json.loads(request.form['data'])
            canvas = getCanvas(self.context)

            try:
                datas = self.parseData(dataRequest)
                data = canvas.saveData(datas)
            except ValueError:
                print "Bad format for data to save."
                data = canvas.dataJson or data

            return data

    def parseData(self, datas):
        matrices = []
        for data in datas:
            for matrice in data['matrices']:
                matrices.append(matrice)
        return matrices

