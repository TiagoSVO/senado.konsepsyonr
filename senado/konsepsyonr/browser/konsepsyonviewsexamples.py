# coding=utf-8

from five import grok
from zope.interface import Interface
from senado.konsepsyonr.interfaces import ISenadoKonsepsyonrAddon, IKonsepsyonViewInterface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer

grok.layer(ISenadoKonsepsyonrAddon)
grok.templatedir("templates")

class ViewExampleKonsepsyon(grok.View):
    """
    * Um exemplo de definição de configuração padrão de uma view Konsepsyon

    * Configurações necessárias:

    --------------------------------
    grok.layer(IDefaultBrowserLayer) #Define layer para ser visualizada em todos os sites
    grok.name('viewglobal')
    grok.require('zope2.View')
    # grok.template('global')
    grok.context(Interface)
    grok.classProvides(IKonsepsyonViewInterface)

    icon = '++resource++senado.konsepsyon/img/kview_default_icon_01.png'
    alias = 'View de Exemplo'

    def update(self):
        pass

    def render(self, **kwargs):
        # => Ou esta pode ser substituida por uma template descomentando a linha com o grok.template
        return "Global!!!"
    ---------------------------------

    """
    grok.name('viewglobal')
    grok.require('zope2.View')
    grok.template('konsepsyonviewexample')

    grok.context(Interface)
    grok.layer(IDefaultBrowserLayer) #Define layer para ser visualizada em todos os sites
    grok.classProvides(IKonsepsyonViewInterface)

    icon = '++resource++senado.konsepsyon/img/kview_default_icon_01.png'
    alias = 'View Global Example'

    def update(self):
        context = self.context
        self.title = context.Title()
        self.description = context.getRawDescription() if getattr(context, 'getRawDescription', None) else getattr(context, 'description', None) or ''
        self.text = context.getRawText() if getattr(context, 'getRawText', None) else getattr(context, 'text', None) or ''

