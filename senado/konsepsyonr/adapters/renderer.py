# coding=utf-8
import json

from five import grok
from zope.interface import Interface, implements
from senado.konsepsyonr.interfaces import ISenadoKonsepsyonrAddon

grok.layer(ISenadoKonsepsyonrAddon)

class IRenderer(Interface):

    def renderSystem(self):
        pass

    def renderOrgan(self):
        pass

    def renderCell(self):
        pass

    def renderOrganelle(self):
        pass

    def renderPayload(self):
        pass

class Renderer(object):
    """
    Adapter que é responsável pela renderização seriada da estrutura em grid Bootstrap do canvas
    traduzida para python a partir da função que renderiza em javascript no componente Konsepsyon.
    """
    implements(IRenderer)

    def renderSystem(self, obj, output):
        """
        Função que renderiza todo o Systema de grid. A raiz da estrutura.
        Tem como filho as tags com classe 'row'.
        """
        output = output or ''
        for kid in obj:
            if(kid.get('k')):
                output += '<div class="row">' + self.renderOrgan(kid.get('k')) + '</div>'
        return output

    def renderOrgan(self, obj):
        """
        Função que renderiza as 'rows' da grid. As linhas da estrutura.
        Tem como filhos as tags com classe 'col' ou 'tissue'.
        """
        output = ''
        for kid in obj:
            if(kid.get('k')):
                output += '<div class="' + kid.get('klass') + '"><div class="row">' + self.renderOrgan(kid.get('k')) + '</div></div>'
            else:
                output += '<div class="' + kid.get('klass') + '">' + self.renderCell(kid.get('p')) + '</div>'
        return output

    def renderCell(self, obj):
        """
        Função que renderiza as 'cols' da grid. As colunas da estrutura.
        Tem como filhos as tags com classe 'organelle'.
        """
        output = ''
        if(obj):
            for kid in obj:
                output += '<div class="m-organelle">' + self.renderOrganelle(kid) + '</div>'
        return output

    def renderOrganelle(self, obj):
        """
        Função que renderiza as 'wrappers' da grid. As tags que envolvem o conteúdo que se deseja apresentar na estrutura.
        Tem como filhos as tags que a view renderizada contém.
        """
        output = ''
        output += self.renderPayload(payload=obj)
        return output

    def renderPayload(self, **kwargs):
        """
        Função que renderiza as tags das views. O HTML da view a ser renderizada.
        """
        payload = kwargs.get('payload') or ''
        return str(json.dumps(payload))
