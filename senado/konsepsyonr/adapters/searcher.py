# coding=utf-8
import json
import copy

from five import grok
from zope.interface import Interface, implements
from senado.konsepsyonr.interfaces import ISenadoKonsepsyonrAddon, IKonsepsyonViewInterface

from zope.publisher.interfaces.browser import IBrowserRequest
from plone.app.customerize import registration
from plone import api
from Products.CMFCore.utils import getToolByName

grok.layer(ISenadoKonsepsyonrAddon)

class ISearcher(Interface):
    """Interface para implementar e adaptar o Seahcer"""

    def query(self):
        """Retorna a pesquisa recebendo como entrada o texto a ser pesquisado"""

    def listObjects(self):
        """Lista os objetos encontrados a partir da query"""

    def listObjectsViews(self):
        """Lista as views por objetos"""

    def processPathViews(self):
        """Função que busca as paths via Brains verificando
        se a path já existe para aquele brain em especifico"""

    def processViewsListByPortalType(self):
        """Busca as views registradas por portal types especifica de cada brain"""

    def getAllKonsepsyonViews(self):
        """Retorna as views que estão registradas com a interface"""

    def listKonsepsyonViewsByObject(self):
        """Lista as views por objeto"""

    def listKonsepsyonViewsByPortalType(self):
        """Lista as views por Portal Type"""

    def structureObjectInformation(self):
        """Retorna a estrutura de dados formatada do objeto esperado
        como retorno para alimentar o menu do componente Konsepsyon"""

    def structureViewInformation(self):
        """Retorna a estrutura de dados formatada da view esperada
        como retorno para alimentar o menu do componente Konsepsyon"""

class Searcher(object):

    implements(ISearcher)

    def query(self, text, limit_sort=50):
        text = text or ""
        portal = api.portal.get()
        catalog = getToolByName(portal, 'portal_catalog')
        query = catalog.searchResults({'Title': text, 'sort_order': 'reverse', 'sort_on': 'effective', 'sort_limit':limit_sort})[:limit_sort]
        result = json.dumps(self.listObjects(query))
        return result

    def listObjects(self, results):
        listObjects = []
        views_by_type = {}
        avaliable_kviews = self.getAllKonsepsyonViews()
        for brain in results:
            listViews = self.listObjectsViews(brain, avaliable_kviews, views_by_type)
            listObjects.append(self.structureObjectInformation(brain, listViews))
        return listObjects

    def listObjectsViews(self, brain, avaliable_kviews, views_by_type):
        list_views, views_by_type = self.processViewsListByPortalType(brain, avaliable_kviews, views_by_type)
        list_views_to_brain = self.processPathViews(brain, list_views)
        return list_views_to_brain

    def processPathViews(self, brain, list_views):
        processed_list_views = copy.deepcopy(list_views)
        for view in processed_list_views:
            view['src'] = brain.getPath() + '/' + view['id']
        return processed_list_views

    def processViewsListByPortalType(self, brain, avaliable_kviews, views_by_type={}):
        views = []
        portal_type = brain[26]
        if portal_type in views_by_type:
            views = views_by_type[portal_type]
        else:
            views = self.listKonsepsyonViewsByPortalType(brain, avaliable_kviews, views_by_type)[brain[26]]
            views_by_type[brain[26]] = views
        return views, views_by_type

    def getAllKonsepsyonViews(self):
        retorno = [
            x for x in registration.getViews(IBrowserRequest)
            if IKonsepsyonViewInterface.providedBy(x.factory)
        ]
        return retorno

    def listKonsepsyonViewsByObject(self, obj, views):
        listy = []
        for view in views:
            for interface in view.required:
                if interface.providedBy(obj):
                    listy.append(self.structureViewInformation(view, obj))
        return listy

    def listKonsepsyonViewsByPortalType(self, brain, views, views_by_type):
        listy = []

        if brain[26] in views_by_type:
            listy = views_by_type[brain[26]]
        else:
            obj = brain.getObject()
            for view in views:
                for interface in view.required:
                    if interface.providedBy(obj):
                        listy.append(self.structureViewInformation(view, obj))
        return {brain[26]: listy}

    def structureObjectInformation(self, brain, listViews):
        return {
                "id": brain.UID,
                "icone": brain.getIcon,
                "title": brain.Title,
                "urlObj": brain.getURL(),
                "date": brain.created.strftime("%d/%m/%Y"),
                "time": brain.created.strftime("%H:%M"),
                "details": self.setDetails(None, brain),
                "views": listViews
        }

    def setDetails(self, text, brain=None):
        result = text
        if not text and brain:
            result = "<strong>Este conteudo se encontra em: </strong><br/>" + str(brain.getURL())
        return result

    def structureViewInformation(self, view, obj):
        name = getattr(view, 'name') if hasattr(view, 'name') else 'NO VIEW NAME...'
        icon = getattr(view.factory, 'icon') if hasattr(view.factory,
                                                        'icon') else '++resource++senado.konsepsyon/img/kview_default_icon_01.png'
        alias = getattr(view.factory, 'alias') if hasattr(view.factory, 'alias') else name
        return {'name': alias,
                'icon': icon,
                'type': 'organelle.plonetype',
                'src': str(obj.absolute_url_path()) + '/' + str(view.name),
                'id': str(view.name)}