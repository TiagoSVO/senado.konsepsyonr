# coding=utf-8
import json

import os
from plone import api
from zope.interface import Interface

from five import grok
from plone.dexterity.content import Container

# from plone.supermodel import model
# from plone.autoform import directives
from plone.directives.form.schema import Schema
from zope import schema
from senado.konsepsyonr.adapters.renderer import IRenderer, Renderer
from senado.konsepsyonr.adapters.searcher import ISearcher, Searcher


# class ICanvas(model.Schema):
class ICanvas(Schema):
    """
    Interface que define o Canvas
    """
    # directives.omitted('data')
    data = schema.Text(
        title=(u"Dados para tabulação"),
        required=False,
    )

class Canvas(Container):
    """
    Classe que implementa a interface ICanvas e definição dos métodos
    """
    grok.implements(ICanvas)

    def saveData(self, datas):
        self.data = datas
        return datas

    def dataJson(self):
        """
        Método que retorna o valor do campo data do canvas em formato json
        """
        data = self.data
        jsonData = "[]"
        try:
            jsonData = json.loads(data)
        except ValueError:
            print "It's not a valid string to parse in json"
        return jsonData

    def dataString(self):
        data = self.data
        try:
            data = json.dumps(data)
        except:
            data = data
        return data


    def renderCanvas(self, **kwargs):
        """
        Método que renderiza a grid e as views do canvas
        """
        output = kwargs.get('output') or ''
        context = kwargs.get('context') or self
        if kwargs.get('jsonDatas'):
            jsonDatas = kwargs.get('jsonDatas')
        elif self.data:
            jsonDatas =  json.loads(self.dataString())
        else:
            jsonDatas =  []
        return IRenderer(context).renderSystem(jsonDatas, output)

class CanvasRenderizer(grok.Adapter, Renderer):
    """
    Adaptando o método para renderizar os payloads (os conteúdos e suas respectivas views)
    """
    grok.context(Interface)
    grok.provides(IRenderer)

    def renderSystem(self, obj, output):
        """
        Função que renderiza todo o Systema de grid. A raiz da estrutura.
        Tem como filho as tags com classe 'row'.
        """
        output = output or ''
        for grid in obj:
            if grid:
                for kid in grid:
                    if(kid.get('k')):
                        output += '<div class="row">' + self.renderOrgan(kid.get('k')) + '</div>'
        return output

    def renderPayload(self, **kwargs):
        """
        Método responsável por tratar e retornar a renderização da view de um conteúdp/objeto
        """
        payload = json.loads(kwargs.get('payload')) if kwargs.get('payload') else ''

        src = str(payload.get('src')) if payload.get('src') else None

        if src:
        #     contexto = api.portal.get().unrestrictedTraverse(path_object_render)
        # try:
        #     cardView = contexto.unrestrictedTraverse("@@gridcard-view")
        #     view_content = api.content.get(path=src)
        #     view = view_content.context.unrestrictedTraverse(src, default=None)
            contexto = api.portal.get().unrestrictedTraverse(src)
            view = contexto.context.unrestrictedTraverse(os.path.basename(os.path.normpath(src)), default=None)
            if view:
                has_update = view.update()
                if has_update:
                    view.update()
                has_render = view.render()
                if not has_render:
                    return str(view._render_template())
                return str(has_render)
            else:
                return "No view named konsepsyon was registred for this type..."
        else:
            return "I'm sorry, but the content type has no template registred..."

class CanvasSearcher(grok.Adapter, Searcher):
    """
    Adaptando o método para pesquisar
    """
    grok.context(Interface)
    grok.provides(ISearcher)