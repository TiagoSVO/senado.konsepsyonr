# -*- coding: utf-8 -*-
import json

import logging
from Products.ATContentTypes.lib import constraintypes

from Products.CMFCore.utils import getToolByName
from Products.CMFPlone.utils import _createObjectByType
from plone import api
from plone.i18n.normalizer import IIDNormalizer
from zope.component import getUtility


def bootstrap(context, module, logger):
    """
    Executa os imports do tipo e workflow para disponibilizar
    as respectivas informações antes de qualquer importação
    """
    site = context.getSite()

    profile_id = 'profile-%s:default' % module
    portal_setup = getToolByName(site, 'portal_setup')
    portal_setup.runImportStepFromProfile(profile_id, 'typeinfo')
    portal_setup.runImportStepFromProfile(profile_id, 'workflow')

    vocabularies_data = _loads_json(context, 'vocabularies.json')
    init_data = _loads_json(context, 'init.json')
    indexes_data = _loads_json(context, 'indexes.json')

    vocabularies_data and create_vocabularies(site, vocabularies_data, logger)
    init_data and create_objs(site, init_data, logger)
    indexes_data and import_indexes(module, indexes_data, context)


def add_catalog_indexes(module, wanted, context, logger=None, catalog_id='portal_catalog'):
    """Method to add our wanted indexes to the portal_catalog.
    When called from the import_various method below, 'context' is
    the plone site and 'logger' is the portal_setup logger.  But
    this method can also be used as upgrade step, in which case
    'context' will be portal_setup and 'logger' will be None.
    """

    logger = logger or logging.getLogger(module)

    # Run the catalog.xml step as that may have defined new metadata
    # columns.  We could instead add <depends name="catalog"/> to
    # the registration of our import step in zcml, but doing it in
    # code makes this method usable as upgrade step as well.  Note that
    # this silently does nothing when there is no catalog.xml, so it
    # is quite safe.
    setup = getToolByName(context, 'portal_setup')
    setup.runImportStepFromProfile(
        'profile-%s:default' % module, 'catalog'
    )

    catalog = getToolByName(context, catalog_id)
    indexes = catalog.indexes()

    indexables = []
    for name, meta_type in wanted:
        if name not in indexes:
            catalog.addIndex(str(name), str(meta_type))
            indexables.append(name)
            logger.info("Added %s for field %s.", meta_type, name)

    if indexables:
        logger.info("Indexing new indexes %s.", ', '.join(indexables))
        catalog.manage_reindexIndex(ids=indexables)


def import_indexes(module, wanted, context, catalog_id='portal_catalog'):
    """Import step for configuration that is not handled in xml files.
    """
    # Only run step if a flag file is present
    if context.readDataFile('%s-default.txt' % module) is None:
        return

    site = context.getSite()
    add_catalog_indexes(
        module, wanted, site, context.getLogger(module), catalog_id
    )


def create_vocabularies(context, vocabularios, logger=None):

    normalizer = getUtility(IIDNormalizer)

    vocabulary_tool = getToolByName(context, 'portal_vocabularies')
    current = vocabulary_tool.listVocabularies()
    vocs = [voc for voc in vocabularios if voc['id'] not in current]

    for voc in vocs:
        vocabulary_tool.invokeFactory('SimpleVocabulary', voc['id'], title=voc['title'])
        simple_vocabulary = vocabulary_tool.getVocabularyByName(voc['id'])
        for item in voc.get('itens', []):
            if isinstance(item, (list, tuple)):
                simple_vocabulary.addTerm(*item)
            else:
                simple_vocabulary.addTerm(normalizer.normalize(item), item)
        [simple_vocabulary[obj].reindexObject() for obj in simple_vocabulary]
        logger.info("Vocabulário `%s` criado.", voc['id'])


def create_objs(parent, children, logger):

    for child in children:

        obj = getattr(parent, child['id'], None)

        if not obj:
            obj = _createObjectByType(
                child['portal_type'], parent, str(child['id']), title=child.get('title', child['id'])
            )

        if not obj:
            logger.info('Não foi possível criar o objeto "/%s"' % child['id'])
            continue

        if 'constrains' in child:
            obj.setConstrainTypesMode(constraintypes.ENABLED)
            obj.setLocallyAllowedTypes(child['constrains'])
            obj.setImmediatelyAddableTypes(child['constrains'])

        if 'defaultview' in child:
            if obj.hasProperty('layout'):
                obj.layout = child['defaultview']
            else:
                obj.manage_addProperty('layout', child['defaultview'], 'string')

        if 'state' in child:
            api.content.transition(obj=obj, to_state=child['state'])

        if 'children' in child:
            create_objs(obj, child['children'], logger)


def _loads_json(context, filename):
    data_file = context.readDataFile(filename)
    return json.loads(data_file, encoding='utf-8') if data_file else None
