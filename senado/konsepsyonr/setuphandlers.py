# -*- coding: utf-8 -*-

import setup

profile_id = 'profile-senado.konsepsyonr:default'
module = 'senado.konsepsyonr'


def import_various(context):
    if context.readDataFile(module + '-default.txt') is None:
        return
    logger = context.getLogger(module)
    setup.bootstrap(context, module, logger)