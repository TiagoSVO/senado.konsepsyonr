# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

version = '1.0.0'

setup(
    name="senado.konsepsyonr",
    version=version,
    description="Diagramador simples de conteudos",
    classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
    ],
    author="NTI - Núcleo de Tecnologia da Informação",
    author_email="agencia_tic@senado.gov.br",
    url="",
    license="MIT",
    packages=find_packages(exclude=['ez_setup']),
    namespace_packages=['senado'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'setuptools',
        'five.grok',
        'z3c.jbot',
        'plone.api',
        'plone.app.dexterity [grok]',
        'plone.app.theming',
        'senado.jquery',
    ],
    extras_require={
        'test': ['plone.app.testing']
    },
    entry_points="""
        # -*- Entry points: -*-
        [z3c.autoinclude.plugin]
        target = plone
    """,
)
